CREATE DATABASE IF NOT EXISTS laravel_master;
USE laravel_master;

CREATE TABLE IF NOT EXISTS users(
id          int(255) AUTO_INCREMENT NOT NULL,
role        varchar(255),
name        varchar(255),
surname     varchar(255),
nick        varchar(255),
email       varchar(255),
password    varchar(255),
image       varchar(255),
created_at  datetime,
updated_at  datetime,
CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;

INSERT INTO users VALUES(NULL, 'user', 'Ciro', 'Goyeneche', 'cirog', 'soycirog@gmail.com', '12345', null, CURTIME(), CURTIME(), NULL);
INSERT INTO users VALUES(NULL, 'user', 'Juan', 'Lopez', 'juanl', 'juan@juan.com', '12345', null, CURTIME(), CURTIME(), NULL);
INSERT INTO users VALUES(NULL, 'user', 'Marcos', 'Fernández', 'marcosf', 'marcos@marcos.com', '12345', null, CURTIME(), CURTIME(), NULL);

CREATE TABLE IF NOT EXISTS images(
id              int(255) AUTO_INCREMENT NOT NULL,
user_id         int(255),
image_path      varchar(255),
description     text,
created_at      datetime,
updated_at      datetime,
CONSTRAINT pk_images PRIMARY KEY(id),
CONSTRAINT fk_images_user FOREIGN KEY(user_id) REFERENCES users(id)
)ENGINE=InnoDb;

INSERT INTO images VALUES(NULL, 1,'test.jpg', 'Descripción de la imagen 1', CURDATE(), CURDATE());
INSERT INTO images VALUES(NULL, 1,'playa.jpg', 'Descripción de la imagen 2', CURDATE(), CURDATE());
INSERT INTO images VALUES(NULL, 1,'arena.jpg', 'Descripción de la imagen 3', CURDATE(), CURDATE());
INSERT INTO images VALUES(NULL, 3,'familia.jpg', 'Descripción de la imagen 4', CURDATE(), CURDATE());

CREATE TABLE IF NOT EXISTS comments(
id          int(255) AUTO_INCREMENT NOT NULL,
user_id     int(255),
image_id    int(255),
content     text,
created_at  datetime,
updated_at  datetime,
CONSTRAINT pk_comments PRIMARY KEY(id),
CONSTRAINT fk_comments_user FOREIGN KEY(user_id) REFERENCES users(id),
CONSTRAINT fk_comments_image FOREIGN KEY(image_id) REFERENCES images(id)
)ENGINE=InnoDb;

INSERT INTO comments VALUES(NULL, 2, 4, 'Buena foto de familia!!!', CURTIME(), CURTIME());

CREATE TABLE IF NOT EXISTS likes(
id          int(255) AUTO_INCREMENT NOT NULL,
user_id     int(255),
image_id    int(255),
created_at  datetime,
updated_at  datetime,
CONSTRAINT pk_likes PRIMARY KEY(id),
CONSTRAINT fk_likes_user FOREIGN KEY(user_id) REFERENCES users(id),
CONSTRAINT fk_likes_image FOREIGN KEY(image_id) REFERENCES images(id)
)ENGINE=InnoDb;

INSERT INTO likes VALUES(NULL, 2, 6, CURTIME(), CURTIME());
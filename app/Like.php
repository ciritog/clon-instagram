<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'likes';

    // Many-to-one relationship
    public function image(){
        return $this->belongsTo('\App\Image', 'image_id');
    }

    // Many to one relationship
    public function user(){
        return $this->belongsTo('\App\User', 'user_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    // Table
    protected $table = 'images';

    // One-to-many relationship: COMMENTS
    public function comments(){
        return $this->hasMany('\App\Comment')->orderBy('id', 'desc');
    }

    // One-to-many relationship: LIKES
    public function likes(){
        return $this->hasMany('\App\Like');
    }

    // Many-to-one relationship: USER
    public function user(){
        return $this->belongsTo('\App\User', 'user_id');
    }

}

<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $user = \Auth::user();
        $likes = Like::where('user_id', $user->id)->orderBy('id', 'desc')
                        ->paginate(5);

        return view('like.index', [
            'likes' => $likes
        ]);
    }

    public function like($image_id){
        
        // Get user and image data
        $user = Auth::user();
        
        // Verify if like is already set
        $isset_like = Like::where('user_id', $user->id)
                      ->where('image_id', $image_id)
                      ->count();
        
        if($isset_like == 0){
            $like = new Like();
            $like->user_id = $user->id;
            $like->image_id = (int)$image_id;

            $like->save();

            return response()->json([
                "like" => $like
            ]);
        }else{
            return response()->json([
                "message" => "Like already set"
            ]); 
        }          

    }

    public function dislike($image_id){
        // Get user and image data
        $user = Auth::user();
        
        // Verify if like is already set
        $like = Like::where('user_id', $user->id)
                      ->where('image_id', $image_id)
                      ->first();
        
        if($like){

            // Delete like
            $like->delete();

            // JSON RESPONSE
            return response()->json([
                "like" => $like,
                "message" => "You disliked."
            ]);
        }else{
            return response()->json([
                "message" => "Like does not exist"
            ]); 
        }          
    }
}

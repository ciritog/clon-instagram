<?php

namespace App\Http\Controllers;

use App\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function config(){
        return view('user.config');
    }

    public function index($search = null){
    
        if(!empty($search)){
            $users = User::where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('surname', 'LIKE', '%' . $search . '%')
                            ->orWhere('nick', 'LIKE', '%' . $search . '%')
                            ->orderBy('id', 'desc')->paginate(5);
        }else{
            $users = User::orderBy('id', 'desc')->paginate(5);
        }

        return view('user.index', [
            "users" => $users
        ]);

    }

    public function update(Request $request){

        $user = Auth::user();
        $id = $user->id;

        $validate = $request->validate([
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'nick' => 'required|string|max:255|unique:users,nick,'.$id,
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
        ]);

        $name       = $request->input('name');
        $surname    = $request->input('surname');
        $nick       = $request->input('nick');
        $email      = $request->input('email');

        // Assign new values to the user object
        $user->name = $name;
        $user->surname = $surname;
        $user->nick = $nick;
        $user->email = $email;

        // Upload image
        $image_path = $request->file('image_path');
        
        if($image_path){
            // Image name
            $image_path_name = time() . $image_path->getClientOriginalName();
            
            // Store image in storage/users
            Storage::disk('users')->put($image_path_name, File::get($image_path));

            // Set image column to the image name
            $user->image = $image_path_name;
        }


        // Store values in the DB.
        $user->update();

        return redirect()->route('config')->with('message', 'Datos actualizados correctamente.');

    }

    public function getImage($filename){
        $file = Storage::disk('users')->get($filename);
        return $file;
    }

    public function profile($id){

        $user = User::find($id);

        return view("user.profile", [
            "user" => $user
        ]);

    }
}

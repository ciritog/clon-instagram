<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request){

        // Get data
        $user = \Auth::user();
        $image_id = $request->input('image_id');
        $content = $request->input('content');


        // Validation
        $validate = $this->validate($request, [
            "image_id" => "integer|required",
            "content" => "string|required"
        ]);

        // Save comment
        $comment = new Comment();
        $comment->user_id =  $user->id;
        $comment->image_id = $image_id;
        $comment->content = $content;

        $comment->save();

        return redirect()->route('image.detail', ["id" => $image_id])
                ->with('message', "The comment was published.");
    }

    public function delete($id){
        
        // Get current user data
        $user = \Auth::user();

        // Get comment
        $comment = Comment::find($id);

        // Comprobate if the current user is the comment's user owner.
        if($user && ($comment->user_id == $user->id || $comment->image->user_id == $user->id)){
            
            // Delete comment
            $comment->delete();

            return redirect()->route('image.detail', ["id" => $comment->image->id])
                             ->with('message', "The comment was deleted.");
        }else{
            return redirect()->route('image.detail', ["id" => $comment->image->id])
                             ->with('message', "The comment was not deleted.");
        }

    }

}

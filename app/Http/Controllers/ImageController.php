<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Image;
use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function create()
    {
        return view('image.create');
    }

    public function save(Request $request)
    {

        // Validation
        $validate = $this->validate($request, [
            'description' => 'required',
            'image_path' => 'required|image'
        ]);



        // Get data
        $image_path = $request->file('image_path');
        $description = $request->input('description');

        // Assign data to the model
        $user = \Auth::user();
        $image = new Image();
        $image->user_id = $user->id;
        $image->description = $description;

        // Store image in DB
        if ($image_path) {
            $image_path_name = time() . '_' . $image_path->getClientOriginalName();
            Storage::disk('images')->put($image_path_name, File::get($image_path));

            $image->image_path = $image_path_name;
        }

        $image->save();

        return redirect()->route('home')->with([
            "message" => "The image was successfully uploaded"
        ]);
    }

    public function getImage($filename)
    {
        $file = Storage::disk('images')->get($filename);
        return $file;
    }

    public function detail($id)
    {
        $image = Image::find($id);

        return view("image.detail", [
            "image" => $image
        ]);
    }

    public function delete($id)
    {

        $user = Auth::user();
        $image = Image::find($id);
        $comments = Comment::where('image_id', $id)->get();
        $likes = Like::where('image_id', $id)->get();

        if ($user && $image->user->id == $user->id) {

            // Delete comments
            if ($comments && count($comments) >= 1) {
                foreach ($comments as $comment) {
                    $comment->delete();
                }
            }

            // Delete likes
            if ($likes && count($likes) >= 1) {
                foreach ($likes as $like) {
                    $like->delete();
                }
            }

            // Delete image file
            Storage::disk('images')->delete($image->image_path);

            // Delete image
            $image->delete();
            $message = array("message" => "The image was deleted");
        } else {
            $message = array("message" => "The image was not deleted");
        }

        return redirect()->route('home')->with($message);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $image = Image::find($id);

        if ($user && $image &&  $image->user_id == $user->id) {
            return view('image.edit', [
                "image" => $image
            ]);
        } else {
            return view("home");
        }
    }

    public function update(Request $request)
    {

        // Validation
        $validate = $this->validate($request, [
            'description' => 'required',
            'image_path' => 'image'
        ]);

        $image_id = $request->input('image_id');
        $image_path = $request->file('image_path');
        $description = $request->input('description');

        // Get image object
        $image = Image::find($image_id);
        $image->description = $description;


        // Store image in DB
        if ($image_path) {
            $image_path_name = time() . '_' . $image_path->getClientOriginalName();
            Storage::disk('images')->put($image_path_name, File::get($image_path));

            $image->image_path = $image_path_name;
        }
        
        // Update record in DB
        $image->update();

        return redirect()->route("image.detail", ["id" => $image_id])
                         ->with("message", "Image updated");
    }
}

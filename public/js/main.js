url = "http://proyecto-laravel.com";

window.addEventListener("load", function(){
    
    $('.btn-like').css('cursor', 'pointer');
    $('.btn-dislike').css('cursor', 'pointer');

    // Like button
    function like(){
        $('.btn-like').unbind('click').click(function(){
            $(this).addClass('btn-dislike').removeClass('btn-like');
            $(this).addClass('red-heart').removeClass('far fa-2x fa-heart');
            $(this).addClass('fas fa-2x fa-heart');

            $.ajax({
                url: url + '/like/' + $(this).data('id'),
                type: 'GET',
                success: function(response){
                    if(response.like){
                        //console.log("Le has dado like");
                        window.location.href = url;
                    }
                }
            })

            dislike();
        });
    }
    like();

    // Dislike button
    function dislike(){
        $('.btn-dislike').unbind('click').click(function(){
            $(this).addClass('btn-like').removeClass('btn-dislike');
            $(this).addClass('far').removeClass('red-heart');
            $(this).addClass('fa-2x fa-heart');

            $.ajax({
                url: url + '/dislike/' + $(this).data('id'),
                type: 'GET',
                success: function(response){
                    if(response.like){
                        //console.log("Diste dislike");
                        window.location.href = url;
                    }
                }
            });

            like();
        });
    }
    dislike();

    // Buscador
    $('#buscador').submit(function(e){
        $(this).attr('action', url+'/users/' + $('#buscador #search').val());
        $(this).submit();
    });

});
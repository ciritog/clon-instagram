@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-5 bg-white p-5">
        <div class="col-sm-12 col-md-4">
            <img src="{{ route('user.picture', ["filename" => $user->image]) }}" class="rounded float-right profile-pic" alt="">
        </div>
        <div class="col-sm-12 col-md-8">
            <h1>{{ $user->name }} {{ $user->surname }}</h1>
            <small>{{ '@' . $user->nick }}</small>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-11">
            @include('includes.message')
            <div class="card-deck">
                @foreach ($user->images as $image)
                    <div class="card">
                        <img class="card-img-top" src="{{ route('image.file',['filename'=>$image->image_path]) }}" alt="Card image cap">
                    </div>
                    @endforeach
            </div>
        </div>
    </div>

</div>
@endsection

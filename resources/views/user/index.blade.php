@extends('layouts.app')

@section('content')
<div class="container">
    <h1>All users</h1>
    <form action="{{ route("user.index") }}" method="GET" class="mt-3" id="buscador">
        <div class="row">
            <div class="form-group col">
                <input type="text" id="search" placeholder="Buscar algo..." name="search" class="form-control">
            </div>            
            <div class="form-group col">
                <input type="submit" value="Buscar" class="btn btn-success">
            </div>
        </div>
    </form>
    <hr>
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach ($users as $user)
                <div class="row">
                    <div class="col-4">
                        <img src="{{ route("user.picture", ["filename" => $user->image]) }} " width="100%">
                    </div>
                    <div class="col-8">
                        <h3>{{ $user->name }} {{ $user->surname }}</h3>
                        <div class="clearfix"></div>
                        <a href="{{ route("user.profile", ["id" => $user->id]) }}" class="btn btn-primary">Ir al perfil</a>
                    </div>
                </div>
                <br>
            @endforeach
        </div>
    </div>
    
    <!-- PAGINATION -->
    <div class="clearfix"></div>
    <div class="d-flex justify-content-center">
        {{ $users->links() }}
    </div>

</div>
@endsection

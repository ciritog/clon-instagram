@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @include('includes.message')
                <div class="card pub_image pub_image_detail">
                    <div class="card-header">
                        <div class="container-avatar">
                            @if($image->user->image)
                                <div class="container-avatar">
                                    <img src="{{ route('user.picture',['filename'=>$image->user->image]) }}" class="avatar" />
                                </div>
                            @endif
                        </div>
                        
                        <div class="data-user">
                            {{ $image->user->name . ' ' . $image->user->surname }}
                            <span class="nickname">
                                {{ ' | @' . $image->user->nick }}
                            </span>
                        </div>

                    </div>

                    <div class="card-body">
                        @if ($image->image_path)
                            <div class="image-container image-detail">
                                <img src="{{ route('image.file', ["filename" => $image->image_path]) }}" alt="">
                            </div>
                        @endif
                        <div class="description">
                            <span class="nickname">{{ '@' . $image->user->nick }}</span>
                            <p>
                                {{ $image->description }}
                            </p>
                            @if (Auth::user() && Auth::user()->id == $image->user->id)
                                <a href="{{ route("image.edit", ["id" => $image->id]) }}" class="btn btn-primary">Editar</a>
                                <!--<a href="{{ route('image.delete', ["id" => $image->id]) }}" class="btn btn-danger">Eliminar</a>-->
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter">
                                    Eliminar
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">¿Estás seguro que deseas eliminarla?</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>La imagen no podrá volver a recuperarse.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                <a href="{{ route('image.delete', ["id" => $image->id]) }}" class="btn btn-danger">Si, eliminar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @include('includes.like')
                        <div class="clearfix"></div>
                        <div class="comments">
                            <h2>Comments ({{ count($image->comments) }})</h2>
                            <hr>

                            <form action="{{ route('comment.save')  }}" method="POST">
                                @csrf
                                <input type="hidden" name="image_id" value="{{ $image->id }} ">
                                <p class="p-3">
                                    <textarea class="form-control mb-2 {{ $errors->has('content') ? 'is-invalid' : ''}}" name="content" required></textarea>
                                    @if ($errors->has('content'))
                                        <span class="invalidad-feedback mt-3" role="alert">
                                            <strong>
                                                {{ $errors->first('content') }}
                                            </strong>
                                        </span>
                                @endif
                                </p>

                                <button class="btn btn-success ml-3 mb-3" type="submit">
                                    Enviar
                                </button>
                            </form>

                            <div class="m-4">
                                @foreach ($image->comments as $comment)
                                    @if(Auth::check() && ($comment->user_id == Auth::user()->id || $comment->image->user_id == Auth::user()->id))
                                        <a href="{{ route('comment.delete', ["id" => $comment->id]) }}" class="float-right
                                            btn btn-sm btn-danger">
                                            <i class="fas fa-times"></i>
                                        </a>
                                    @endif
                                    <p><strong>{{ $comment->user->name . " " . $comment->user->surname }}</strong>: {{ $comment->content }}</p>
                                    <hr>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
        </div>
    </div>
    
</div>
@endsection

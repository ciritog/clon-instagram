@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('includes.message')
            @foreach ($images as $image)
                @include('includes.image', ["images" => $images])
            @endforeach
        </div>
    </div>
    
    <!-- PAGINATION -->
    <div class="clearfix"></div>
    <div class="d-flex justify-content-center">
        {{ $images->links() }}
    </div>

</div>
@endsection

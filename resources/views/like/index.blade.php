@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Images you liked</h2>
    <hr>
    <div class="row justify-content-center mt-5">
        <div class="col-md-8">
            @foreach ($likes as $like)
                @include('includes.image', ["image" => $like->image])
            @endforeach
        </div>
    </div>
    
    <!-- PAGINATION -->
    <div class="clearfix"></div>
    <div class="d-flex justify-content-center">
        {{ $likes->links() }}
    </div>

</div>
@endsection

<div class="likes d-inline">

    <?php $user_like = false; ?>
    @foreach ($image->likes as $like)
        @if ($like->user->id == Auth::user()->id)
           <?php $user_like = true; ?>
        @endif
    @endforeach

    @if ($user_like)
        <i class="btn btn-dislike red-heart fas fa-2x fa-heart" data-id="{{ $image->id }}"></i>
        {{ count($image->likes) }}
    @else
        <i class="btn btn-like far fa-2x fa-heart" data-id="{{ $image->id }}"></i>
        {{ count($image->likes) }}
    @endif

    <a href="{{ route('image.detail', ["id" => $image->id]) }}" class="btn btn-warning btn-comments">
        Comments ({{ count($image->comments) }})
    </a>
</div>
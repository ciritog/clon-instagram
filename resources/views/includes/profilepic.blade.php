@if (Auth::user()->image)
    <div class="container-avatar">
        <img src="{{ route('user.picture', ['filename' => Auth::user()->image]) }}" class="mb-3 rounded min-profile-picture ">
    </div>
@endif
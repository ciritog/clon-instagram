<div class="card pub_image">
    <div class="card-header">
        <div class="container-avatar">
            @if($image->user->image)
                <div class="container-avatar">
                    <img src="{{ route('user.picture',['filename'=>$image->user->image]) }}" class="avatar" />
                </div>
            @endif
        </div>
        
        <div class="data-user">
            <a href="{{ route("user.profile", ["id" => $image->user_id]) }}">
                {{ $image->user->name . ' ' . $image->user->surname }}
                <span class="nickname">
                    {{ ' | @' . $image->user->nick }}
                </span>
            </a>
        </div>

    </div>

    <div class="card-body">
        @if ($image->image_path)
            <div class="image-container">
                <img src="{{ route('image.file', ["filename" => $image->image_path]) }}" alt="">
            </div>
        @endif
        <div class="description">
            <span class="nickname">{{ '@' . $image->user->nick }}</span>
            <span class="nickname ml-2">{{ \FormatTime::LongTimeFilter($image->created_at) }}
            </span>
            <p>
                {{ $image->description }}
            </p>
        </div>
        @include('includes.like')
    </div>
</div>